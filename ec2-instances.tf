resource "aws_instance" "bastion" {
  ami = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  key_name = var.keypair_name
  vpc_security_group_ids = [aws_security_group.bastion_sg.id]
  subnet_id = element(aws_subnet.public_subnets.*.id, 0)
  availability_zone = var.availability_zones[0]
  associate_public_ip_address = true

  provisioner "remote-exec" {
    connection {
      host        = self.public_ip
      private_key = file(var.private_key)
      user        = var.ubuntu_user
    }
    inline = ["sudo hostnamectl set-hostname ${var.bastion_hostname}"]
  }

  provisioner "local-exec" {
    when    = destroy
    command = "./scripts/delete-sg.sh ${self.tags.region} ${self.id}"
  }

  tags = {
    Name = "terraform-k8s-bastion"
    region = var.aws_region
  }
}

resource "aws_instance" "runner" {
  depends_on = [aws_instance.bastion, local_file.ssh_jump_conf]
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  key_name      = var.keypair_name
  vpc_security_group_ids = [aws_security_group.runner_sg.id]
  subnet_id = element(aws_subnet.private_subnets.*.id, 0)
  availability_zone = var.availability_zones[0]
  associate_public_ip_address = false

  provisioner "remote-exec" {
    connection {
      bastion_host = aws_instance.bastion.public_ip
      host        = self.private_ip
      private_key = file(var.private_key)
      user        = var.ubuntu_user
    }
    inline = ["sudo hostnamectl set-hostname ${var.gitlab_runner_hostname}"]
  }
  provisioner "local-exec" {
    command = <<EOT
      cd ${var.ansible_folder};
      ansible-playbook -i ${self.private_ip}, \
                       -u ${var.ubuntu_user} playbook-runner.yml \
                       --tags "runner"
    EOT
  }

  tags = {
    Name = "terraform-k8s-gitlab-runner"
  }
}

resource "aws_instance" "master" {
  depends_on = [aws_instance.bastion, ]
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.cluster_instance_type
  key_name      = var.keypair_name
  vpc_security_group_ids = [aws_security_group.k8s_sg.id]
  subnet_id = element(aws_subnet.private_subnets.*.id, 0)
  availability_zone = var.availability_zones[0]
  iam_instance_profile = aws_iam_instance_profile.master_profile.name
  associate_public_ip_address = false

  provisioner "remote-exec" {
    connection {
      bastion_host = aws_instance.bastion.public_ip
      host        = self.private_ip
      private_key = file(var.private_key)
      user        = var.ubuntu_user
    }
    inline = ["sudo hostnamectl set-hostname $(curl -s ${var.aws_hostname_api})"]
  }
  provisioner "local-exec" {
    command = <<EOT
      cd ${var.ansible_folder};
      ansible-playbook -i ${self.private_ip}, \
                       -u ${var.ubuntu_user} playbook-k8s-image.yml \
                       --tags "k8s"
    EOT
  }

  tags = {
    Name = "terraform-k8s-master"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

// Create aws image
resource  "aws_ami_from_instance" "k8s-ami" {
  name               = var.ami_name
  source_instance_id = aws_instance.master.id
  depends_on = [aws_instance.master]
  tags = {
    Name = var.ami_name
  }
}

resource "aws_instance" "worker" {
  depends_on = [aws_instance.bastion, aws_ami_from_instance.k8s-ami]
  ami           = data.aws_ami.k8s_ami.id
  instance_type = var.cluster_instance_type
  key_name      = var.keypair_name
  vpc_security_group_ids = [aws_security_group.k8s_sg.id]
  subnet_id = element(aws_subnet.private_subnets.*.id, 1)
  availability_zone = var.availability_zones[1]
  iam_instance_profile = aws_iam_instance_profile.worker_profile.name
  associate_public_ip_address = false

  provisioner "remote-exec" {
    connection {
      bastion_host = aws_instance.bastion.public_ip
      host        = self.private_ip
      private_key = file(var.private_key)
      user        = var.ubuntu_user
    }
    inline = ["sudo hostnamectl set-hostname $(curl -s ${var.aws_hostname_api})"]
  }
  tags = {
    Name = "terraform-k8s-worker"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

locals {
  template_runners = templatefile("${path.module}/templates/all.toml", {
    bastion_ip = aws_instance.bastion.public_ip,
    user = var.ubuntu_user
  })
}

resource "local_file" "ssh_jump_conf" {
  content = local.template_runners
  filename = "ansible/group_vars/all.yml"
}

locals {
  template_inventory_init = templatefile("${path.module}/templates/inventory.toml", {
    ansible_user         = var.ubuntu_user,
    master_ip            = aws_instance.master.private_ip,
    worker_ip            = aws_instance.worker.private_ip,
    cloud_provider       = var.cloud_provider,
    pod_network_cidr     = var.pod_network_cidr,
    service_network_cidr = var.service_network_cidr,
    runner_ip            = aws_instance.runner.private_ip,
    hosted_zone_id       = aws_route53_zone.rt53_zone.zone_id
  })
}

resource "local_file" "inventory_conf" {
  content = local.template_inventory_init
  filename = "${var.ansible_folder}/inventory.ini"
}

resource "null_resource" "install_k8s_cluster" {
  depends_on = [
    local_file.inventory_conf,
    local_file.ssh_jump_conf]

  provisioner "local-exec" {
    command = <<EOT
      cd ${var.ansible_folder};
      ansible-playbook -i inventory.ini playbook-k8s.yml
    EOT
  }
}

output "instance_ips" {
  value = {
    master_private_ip        = aws_instance.master.private_ip
    worker_private_ip        = aws_instance.worker.private_ip
    gitlab_runner_private_ip = aws_instance.runner.private_ip
    bastion_public_ip        = aws_instance.bastion.public_ip
  }
}

output "utilities" {
  value = {
    connect_to_master = "ssh -J ubuntu@${aws_instance.bastion.public_ip} ubuntu@${aws_instance.master.private_ip}"
    copy_kube_conf = "ssh -J ubuntu@${aws_instance.bastion.public_ip} ubuntu@${aws_instance.master.private_ip} 'cat ~/.kube/config' | base64 |  xclip -sel c"
  }
}