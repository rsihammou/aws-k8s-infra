#!/bin/bash

region=$1;
bastion_id=$2;
vpc_id=$(aws ec2 describe-vpcs --region $region \
         --filters Name=tag:Name,Values="terraform-k8s" |
         jq --raw-output ".Vpcs[0].VpcId");

# Delete alb created by external-dns
for name in $(aws elb describe-load-balancers --region $region |
              jq --raw-output ".LoadBalancerDescriptions[].LoadBalancerName");
do
  aws elb delete-load-balancer --region $region --load-balancer-name $name;
done

# Terminate bastion ec2
aws ec2 terminate-instances --region $region --instance-ids $bastion_id;

# Wait for terraform to terminate other instances
for id in $(aws ec2 describe-instances --region $region --filters  \
            Name=vpc-id,Values="${vpc_id}" |
            jq --raw-output ".Reservations[].Instances[].InstanceId");
do
  while [ $(aws ec2 describe-instances --region $region --instance-ids $id \
            | jq --raw-output ".Reservations[].Instances[].State.Name") != "terminated" ];
  do
    echo "Wait until instances deletion to complete";
    sleep 5	
  done  
done

# Delete securityGroups
for id in $(aws ec2 describe-security-groups --region $region \
            --filters  Name=vpc-id,Values="${vpc_id}" \
            Name=tag:kubernetes.io/cluster/kubernetes,Values="owned" \
            | jq --raw-output ".SecurityGroups[].GroupId");
do
  aws ec2 delete-security-group --region $region --group-id $id;
done

