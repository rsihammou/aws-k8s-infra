resource "aws_iam_role" "master_role" {
  name = "k8s_iam_master_role"
  assume_role_policy = file("${path.module}/policies/iam_ec2_role.json")
  tags = {
    Name = "terraform-k8s-master"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

resource "aws_iam_role" "worker_role" {
  name = "k8s_iam_worker_role"
  assume_role_policy = file("${path.module}/policies/iam_ec2_role.json")
  tags = {
    Name = "terraform-k8s-worker"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}
resource "aws_iam_instance_profile" "master_profile" {
  name = "k8s_iam_master_profile"
  role = aws_iam_role.master_role.name

}

resource "aws_iam_instance_profile" "worker_profile" {
  name = "k8s_iam_worker_profile"
  role = aws_iam_role.worker_role.name
}

resource "aws_iam_role_policy" "master_policies" {
  count = length(var.master_policy_file_names)
  name = "k8s_iam_master_policy_${count.index}"
  role = aws_iam_role.master_role.id
  policy = file("${path.module}/policies/${element(var.master_policy_file_names, count.index)}")
}

resource "aws_iam_role_policy" "worker_policies" {
  count = length(var.worker_policy_file_names)
  name = "k8s_iam_worker_policy_${count.index}"
  role = aws_iam_role.worker_role.id
  policy = file("${path.module}/policies/${element(var.worker_policy_file_names, count.index)}")
}

resource "aws_iam_policy" "cert_manager_policy" {
  name = "cert_manager_policy"
  policy = file("${path.module}/policies/cert_manager_policy.json")
}

resource "aws_iam_user_policy_attachment" "policy_attachment" {
  user = var.aws_user_name
  policy_arn = aws_iam_policy.cert_manager_policy.arn
}


