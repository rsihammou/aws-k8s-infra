# provision app vpc
resource "aws_vpc" "main_vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  tags = {
    Name = "terraform-k8s"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

# create public subnets
resource "aws_subnet" "public_subnets" {
  count = length(var.public_subnet_cidrs)
  cidr_block = element(var.public_subnet_cidrs, count.index )
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = element(var.availability_zones, count.index)

  tags = {
    Name = "terraform-k8s-public-${count.index + 1}"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

# create private app subnets
resource "aws_subnet" "private_subnets" {
  count = length(var.private_subnet_cidrs)
  cidr_block = element(var.private_subnet_cidrs, count.index )
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = element(var.availability_zones, count.index)
  tags = {
    Name = "terraform-k8s-private-${count.index + 1}"
  }
}

# create igw
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main_vpc.id


  tags = {
    Name = "terraform-k8s"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}


resource "aws_eip" "eip" {
  count = length(aws_subnet.public_subnets)
  vpc = true
  tags = {
    Name = "terraform-k8s-subnet-${count.index + 1}"
  }
}

resource "aws_nat_gateway" "ngw" {
  depends_on = [aws_internet_gateway.igw]
  count = length(aws_subnet.public_subnets)
  allocation_id = element(aws_eip.eip.*.id, count.index)
  subnet_id = element(aws_subnet.public_subnets.*.id, count.index)
  tags = {
    Name = "terraform-k8s-subnet-${count.index + 1}"
  }
}