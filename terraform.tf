terraform {
  backend "s3" {
    bucket = "fr.alltech.terraform"
    key    = "terraform/state"
    region = "eu-west-3"
  }
}
